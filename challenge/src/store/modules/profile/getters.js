/* ============
 * Getters for the auth module
 * ============
 *
 * The getters that are available on the
 * auth module.
 */

export default {
	bump: state => {
		return state.currentIndex + 10;
	}
};
