/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import Vue from "vue";
import store from "@/store";
import * as types from "./mutation-types";

import axios from "axios";

export const inc = ({ commit }) => {
  commit(types.INC);
};

export const dec = ({ commit }) => {
  commit(types.DEC);
};

//only for test, should commit an action
export const home = ({ commit }) => {
  Vue.router.push({
    name: "home.index"
  });
};

export const fetch = async ({ commit }) => {
  let res = await axios.get("https://itunes.apple.com/search?origin=*&term=acdc&entity=album");
  console.log("fetched: ", res);
  console.log("nb of results: ", res.data.resultCount);
  console.log("artist name: ", res.data.results[0].artistName);
  commit(types.FETCHED, res.data.results);
};

export const clear = ({ commit }) => {
  console.log("Clearing results..");
  commit(types.CLEAR);
};

export default {
  inc,
  dec,
  home,
  fetch,
  clear
};
